package com.myurbex.utils;//package com.example.accessingdatamysql;
//
//import org.springframework.boot.web.servlet.ServletContextInitializer;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//
//@Configuration
//public abstract class ServletContextConfiguration extends SpringBootServletInitializer {
//
//    @Bean
//    public ServletContextInitializer myUrbexContext() {
//        return new ServletContextInitializer() {
//            @Override
//            public void onStartup(ServletContext servletContext) throws ServletException {
//                servletContext.setInitParameter("reloadable", "true");
//                servletContext.setDocBase("MyUrbex");
//                servletContext.setContextPath("/MyUrbex");
//            }
//        };
//    }
//
//    @Bean
//    public ServletContextInitializer imgsContext() {
//        return new ServletContextInitializer() {
//            @Override
//            public void onStartup(ServletContext servletContext) throws ServletException {
//                servletContext.setDocBase("C:/TomCat9/webapps/imgs");
//                servletContext.setContextPath("/imgs");
//            }
//        };
//    }
//
//    @Bean
//    public ServletContextInitializer iconsContext() {
//        return new ServletContextInitializer() {
//            @Override
//            public void onStartup(ServletContext servletContext) throws ServletException {
//                servletContext.setDocBase("C:/TomCat9/webapps/icons");
//                servletContext.setContextPath("/icons");
//            }
//        };
//    }
//}
