package com.myurbex.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

public class PropertiesHelper {

	public static Properties getProperties()
	{
		Properties prop = new Properties();
		try (InputStream input = PropertiesHelper.class.getClassLoader().getResourceAsStream("resources/config.properties")) {
			prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
		return prop;
	}
	
	public enum OSType {
	    Windows, MacOS, Linux, Other
	  };

	protected static OSType detectedOS;

	public static OSType getOperatingSystemType() {
	  if (detectedOS == null) {
		  String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
	  if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
		  detectedOS = OSType.MacOS;
	  } else if (OS.indexOf("win") >= 0) {
		  detectedOS = OSType.Windows;
	  } else if (OS.indexOf("nux") >= 0) {
	      detectedOS = OSType.Linux;
	    } else {
	      detectedOS = OSType.Other;
	    }
	  }
	  return detectedOS;
	}
	
}
