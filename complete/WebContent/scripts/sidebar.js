function loadSideBar(user_type_id){
	loadVisited(user_type_id);
	loadIsUrbex(user_type_id);
	loadTypes(user_type_id);
	loadStatuses(user_type_id);
	loadCountries(user_type_id);
	loadRegions(user_type_id);
	loadDepartments(user_type_id);
  	showFilter();
}

function loadVisited(user_type_id){	
	$.ajax({
	    url:'PopulateVisited',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {	
	        var HTML = ""; 	
	        $.each(json, function(key, data) { 	 	           
	           HTML += "<input type='checkbox' name='checkVisited' value='" + data.nickname + "' id='" + (data.nickname != null && data.nickname != '' ? data.nickname : 'undefined') +  
	        		"' onchange='checkForUpdate();' checked='true'>"
	       			+ "<label for='" + (data.nickname != null && data.nickname != '' ? data.nickname : 'undefined') + "'>"  + (data.nickname != null && data.nickname != '' ? data.nickname : 'Non Visit&eacute;') + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#visited').append(HTML); 	        
	    },
        error : function(e) {
            
        }
	  });
}

function loadIsUrbex(user_type_id){
	var totalCount = 0;
	$.ajax({
	    url:'PopulateIsUrbex',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {	    	
	        var HTML = ""; 	        
	        $.each(json, function(key, data) { 	 
	           totalCount += parseInt(data.count, 10);
	           HTML += "<input type='checkbox' name='checkUrbex' value='" + data.is_urbex + "' id='" + data.is_urbex +  
	        		"' onchange='checkForUpdate()' checked='true'>"
	       			+ "<label for='" + data.is_urbex + "'>"  + (data.is_urbex ? 'Urbex' : 'Non Urbex') + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#isurbex').append(HTML); 
	        $('#count').append("Nombre de lieux : " + totalCount);
	    },
        error : function(e) {
            
        }
	  });
}

function loadTypes(user_type_id){
	$.ajax({
	    url:'PopulateTypes',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = ""; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<input type='checkbox' name='checkType' value='" + data.code_type + "' id='" + data.code_type +  
	        		"' onchange='checkForUpdate()' checked='true'>"
	       			+ "<label for='" + data.code_type + "'>"  + data.label + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#types').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadStatuses(user_type_id){
	$.ajax({
	    url:'PopulateStatus',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = ""; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<input type='checkbox' name='checkStatus' value='" + data.code_status + "' id='" + data.code_status +  
	        		"' onchange='checkForUpdate()' checked='true'>"
	       			+ "<label for='" + data.code_status + "'>"  + data.label + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#statuses').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadCountries(user_type_id){
	$.ajax({
	    url:'PopulateCountries',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = ""; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<input type='checkbox' name='checkCountry' value='" + data.fr_name + "' id='" + data.fr_name +  
	        		"' onchange='checkForUpdate()' checked='true'>"
	       			+ "<label for='" + data.fr_name + "'>" + data.fr_name + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#countries').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadRegions(user_type_id){
	$.ajax({
	    url:'PopulateRegions',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = ""; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<input type='checkbox' name='checkRegion' value='" + data.id + "' id='r" + data.id +  
	        		"' onchange='checkForUpdate()' checked='true'>"
	       			+ "<label for='r" + data.id + "'>" + data.name + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#regions').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadDepartments(user_type_id){
	$.ajax({
	    url:'PopulateDepartments',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = ""; 	        
	        $.each(json, function(key, data) { 	
	           HTML += "<input type='checkbox' name='checkDept' value='" + data.id + "' id='d" + data.id +  
	        		"' onchange='checkForUpdate()' checked='true'>"
	       			+ "<label for='d" + data.id + "'>" + data.name + " (" + data.count + ")" + "</label><br>"; 
	        });
	        $('#depts').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function updateVisited(ids){
	$.ajax({
	    url:'UpdateVisited',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {	 
	        $.each(json, function(key, data) { 	
	        	$("label[for='" + (data.nickname != null && data.nickname != '' ? data.nickname : 'undefined') + "']").empty();
	        	$("label[for='" + (data.nickname != null && data.nickname != '' ? data.nickname : 'undefined') + "']").append((data.nickname != null && data.nickname != ''? data.nickname : 'Non Visit&eacute;') + " (" + data.count + ")");
	        });	        
	    },
        error : function(e) {
            
        }
	  });
}

function updateIsUrbex(ids){
	$.ajax({
	    url:'UpdateIsUrbex',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {	    	        
	        $.each(json, function(key, data) { 
	           $("label[for='" + data.is_urbex + "']").empty();
	           $("label[for='" + data.is_urbex + "']").append((data.is_urbex ? 'Urbex' : 'Non Urbex') + " (" + data.count + ")");
	        });
	    },
        error : function(e) {
            
        }
	  });	
}

function updateTypes(ids){
	$.ajax({
	    url:'UpdateTypes',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {     
	        $.each(json, function(key, data) { 	    
	           $("label[for='" + data.code_type + "']").empty();
	           $("label[for='" + data.code_type + "']").append(data.label + " (" + data.count + ")");	           
	        });
	    },
        error : function(e) {
            
        }
	  });
}

function updateStatuses(ids){
	$.ajax({
	    url:'UpdateStatus',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {	        
	        $.each(json, function(key, data) { 	    
	           $("label[for='" + data.code_status + "']").empty();
	           $("label[for='" + data.code_status + "']").append(data.label + " (" + data.count + ")");	           
	        });
	    },
        error : function(e) {
            
        }
	  });
}

function updateCountries(ids){
	$.ajax({
	    url:'UpdateCountries',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {  
	        $.each(json, function(key, data) { 
	           $("label[for='" + data.fr_name + "']").empty();
	           $("label[for='" + data.fr_name + "']").append(data.fr_name + " (" + data.count + ")");	  
	        });
	    },
        error : function(e) {
            
        }
	  });
}

function updateRegions(ids){
	$.ajax({
	    url:'UpdateRegions',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {  
	        $.each(json, function(key, data) { 
	           $("label[for='r" + data.id + "']").empty();
	           $("label[for='r" + data.id + "']").append(data.name + " (" + data.count + ")");	  
	        });
	    },
        error : function(e) {
            
        }
	  });
}

function updateDepartments(ids){
	$.ajax({
	    url:'UpdateDepartments',
	    type:'POST',
	    data: {
	    	ids: ids
	    },
	    dataType: 'json',
	    success: function( json ) {  
	        $.each(json, function(key, data) { 
	           $("label[for='d" + data.id + "']").empty();
	           $("label[for='d" + data.id + "']").append(data.name + " (" + data.count + ")");	  
	        });
	    },
        error : function(e) {
            
        }
	  });
}

function checkForUpdate(){
	filter();
	var ids = getUnvisibleMarkers();
	updateVisited(ids);
	updateIsUrbex(ids);
	updateTypes(ids);
	updateStatuses(ids);
	updateCountries(ids);
	updateRegions(ids);
	updateDepartments(ids);
}
