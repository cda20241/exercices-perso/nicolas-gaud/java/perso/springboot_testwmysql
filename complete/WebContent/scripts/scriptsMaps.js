
  var map;
  var markers = [];
  var highestZIndex = 0;
  var tmpMarker = new google.maps.Marker();
  var bounds;
  
  function startTime(){
	  var today = new Date();
	  var h = today.getHours();
	  var m = today.getMinutes();
	  var s = today.getSeconds(); 
	  return [ h, m, s ].join(':')
	}
  
  function initialize(user_type_id, marker_id) {   

	var mapOptions = {
        center: new google.maps.LatLng(48.858386, 2.336758),
		mapTypeId: google.maps.MapTypeId.HYBRID,
		streetViewControl: true
	};
	map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
	   autoCompleteMap(user_type_id);
	   loadSideBar(user_type_id);	
	   populateMap(user_type_id, marker_id);	
  }

  
  function populateMap(user_type_id, marker_id){	
	  $.getJSON('PopulateMap', {user_type_id : user_type_id})
	  .done(function(json){
		var originalMapCenter = new google.maps.LatLng(48.858386, 2.336758);
		var infowindow;	
		
		$.each(json, function(key, data) {  
		
		var id = data.id;
		var name = data.name;
		var description = data.description;
		var latitude = data.latitude;
		var longitude = data.longitude;
		var is_urbex = data.is_urbex;	
		var maps_link = data.maps_link;
		var type_label = data.type_label;
		var type_code = data.type_code;	
		var status_label = data.status_label;
		var status_code = data.status_code;
		var motif_label = data.motif_label;
		var nickname = data.nickname;
		var department_name = data.department_name;
		var department_id = data.department_id;
		var region_name = data.region_name;
		var region_id = data.region_id;
		var country_name = data.country_name;
		var country_id = data.country_id;
		var first_image = data.first_image;		
		var date_visited = data.date_visited;

		var latLng = new google.maps.LatLng(latitude, longitude); 
		
		var image = getIcon(type_code, status_code, nickname, motif_label);	
		var icon = {
				    url: image, 
				    scaledSize: new google.maps.Size(30, 30), 
				    origin: new google.maps.Point(0,0), 
				    anchor: new google.maps.Point(0, 0)
				};	
				     
		var marker = new google.maps.Marker({
			position: latLng,
			title: name,
			id: id,
			name: name,
			description: description,
			latitude: latitude,
			longitude: longitude,
			is_urbex: is_urbex,
			maps_link: maps_link,
			type_label: type_label,
			type_code: type_code,
			status_label: status_label,
			status_code: status_code,
			motif_label: motif_label,
			nickname: nickname,
			department_name: department_name,
			department_id: department_id,
			region_name: region_name,
			region_id: region_id,
			country_name: country_name,
			country_id: country_id,
			first_image: first_image,
			date_visited: date_visited,
			icon: icon,
		    infowindow: infowindow,
		    zIndex: 1
		});			
		//alert(icon.url)   
        marker.set("myZIndex", marker.getZIndex());

        google.maps.event.addListener(marker, "mouseover", function() {
            getHighestZIndex();
            this.setOptions({zIndex:highestZIndex+1});
        });
        
		google.maps.event.addListener(marker, 'click', function() {	
			tmpMarker.setMap(null);
            getHighestZIndex();
            this.setOptions({zIndex:highestZIndex+1});
			if (infowindow) {
			    infowindow.close();
			}			
			infowindow = new google.maps.InfoWindow({
			    content: getInfoWindow(marker, user_type_id),
			    position: originalMapCenter
			  });
			if(!marker.open){
			    infowindow.open(map,marker);
			    marker.open = true;
			}
			else{
			    infowindow.close();
			    marker.open = false;
			}
			google.maps.event.addListener(map, 'click', function() {
				tmpMarker.setMap(null);
			    infowindow.close();
			    marker.open = false;
			});
			map.setCenter(marker.getPosition());
		}); 
		markers.push(marker);
	  	});	  
		
	  setMapOnAll(map);
	  
	  bounds = new google.maps.LatLngBounds();
	  for (var i = 0; i < markers.length; i++) {
		  bounds.extend(markers[i].getPosition());
	  } 
	  map.setCenter(bounds.getCenter());
	  map.fitBounds(bounds);
	  if (marker_id != null && marker_id != '')
		  goToMarker(marker_id);
	})
 }
    
  
function getInfoWindow(marker, user_type_id){
	var infowincontent = document.createElement('div');
	
	var strongTitle = document.createElement('strong');    				 
	strongTitle.textContent = marker.name;
	if (marker.department_id != '' && marker.department_id != null){
		strongTitle.textContent += ' (' + marker.department_id + ' - ' + marker.department_name + ')';
	}
	else if (marker.country_name != '' && marker.country_name != null){
		strongTitle.textContent += ' (' + marker.country_name + ')';
	}
	infowincontent.appendChild(strongTitle);
	infowincontent.appendChild(document.createElement('br'));
	infowincontent.appendChild(document.createElement('br'));		
	
	if (marker.description != '' && marker.description != null){
		var strongDescription = document.createElement('strong');    	
		strongDescription.textContent = 'Description : ';
		infowincontent.appendChild(strongDescription);
		var txtDescription = document.createElement('text');			
		txtDescription.textContent = marker.description;
		infowincontent.appendChild(txtDescription);
		infowincontent.appendChild(document.createElement('br'));
		infowincontent.appendChild(document.createElement('br'));
	}
	
	var strongType = document.createElement('strong');   
	strongType.textContent = 'Type : ';
	infowincontent.appendChild(strongType);
	var txtType = document.createElement('text');		
	txtType.textContent = marker.type_label;
	infowincontent.appendChild(txtType);
	infowincontent.appendChild(document.createElement('br'));
	infowincontent.appendChild(document.createElement('br'));
	
	var strongStatus = document.createElement('strong');   
	strongStatus.textContent = 'Status : ';
	infowincontent.appendChild(strongStatus);
	var txtStatus = document.createElement('text');		
	txtStatus.textContent = marker.status_label;
	if (marker.motif_label != '' && marker.motif_label != null){
		txtStatus.textContent += ' - ' + marker.motif_label;
	}
	infowincontent.appendChild(txtStatus);
	infowincontent.appendChild(document.createElement('br'));
	infowincontent.appendChild(document.createElement('br'));	
	
	if (marker.nickname != '' && marker.nickname != null){
		var strongDate = document.createElement('strong');   
		strongDate.textContent = 'Date visite : ';
		infowincontent.appendChild(strongDate);
		var txtDate = document.createElement('text');		
		txtDate.textContent = marker.date_visited;
		infowincontent.appendChild(txtDate);
		infowincontent.appendChild(document.createElement('br'));
		infowincontent.appendChild(document.createElement('br'));	
	}
	
	var url = document.createElement('a'); 
	url.textContent = 'Details';
	url.setAttribute('href', '/MyUrbex/markerdetails?id=' + marker.id);
	url.setAttribute('target', '_blank');
	url.setAttribute('onclick', 'filter()');
	infowincontent.appendChild(url);
	infowincontent.appendChild(document.createElement('br'));
	infowincontent.appendChild(document.createElement('br'));
	
	if (marker.first_image != '' && marker.first_image != null){
		var aImage = document.createElement('a'); 
		aImage.setAttribute('href', '/MyUrbex/markerdetails?id=' + marker.id);
		aImage.setAttribute('target', '_blank');
		aImage.setAttribute('onclick', 'filter()');
		var image = document.createElement('img');
		image.setAttribute('src', '/imgs/' + marker.first_image);
		image.setAttribute('width', '305');
		image.setAttribute('height', '230');
		image.setAttribute('alt', '');
		aImage.appendChild(image)
		infowincontent.appendChild(aImage);
		infowincontent.appendChild(document.createElement('br'));
		infowincontent.appendChild(document.createElement('br'));
	}	
	
	var link = document.createElement('a'); 
	link.textContent = 'Lien google maps';
	link.setAttribute('href', marker.maps_link);
	link.setAttribute('target', '_blank');
	infowincontent.appendChild(link);
	infowincontent.appendChild(document.createElement('br'));
	infowincontent.appendChild(document.createElement('br'));
	
	if (parseInt(user_type_id) >= 4){
		var modify = document.createElement('a'); 
		modify.textContent = 'Modifier le lieu';
		modify.setAttribute('href', '/MyUrbex/modifymarker?id=' + marker.id);
		infowincontent.appendChild(modify);
	}
	
	return infowincontent;
}  
  
function getIcon(type_code, status_code, nickname, motif_label){
	  var result = '/icons/';
	  var color = '';
	  var label = '';	  
	  if (nickname != '' && nickname != null){
		  color = 'GRE';
	  }
	  else{
		  switch(status_code){
		  	case 'D': color = 'RED'; break;
		  	case 'O': color = 'BLU'; break;
		  	case 'I': color = 'ORA'; break;
		  }
	  }
	  if(motif_label != '' && motif_label != null && motif_label == 'Officiel'){
		  label = 'OFF_';
	  }
	  else{
		  switch(type_code){
			  case 'ABB': label = 'REG_'; break;
			  case 'CHA': label = 'REG_'; break;
			  case 'EGL': label = 'REG_'; break;
			  case 'CAT': label = 'REG_'; break;
			  case 'CHT': label = 'MAI_'; break;
			  case 'MAI': label = 'MAI_'; break;
			  case 'RUC': label = 'RUC_'; break;
			  case 'CIB': label = 'CIB_'; break;
			  case 'CIV': label = 'CIV_'; break;
			  case 'CIA': label = 'CIA_'; break;
			  case 'USI': label = 'USI_'; break;
			  case 'CIM': label = 'REG_'; break;
			  case 'DIS': label = 'DIS_'; break;
			  case 'BAR': label = 'USI_'; break;
			  case 'CIT': label = 'CIT_'; break;
			  case 'MUS': label = 'MUS_'; break;
			  case 'HOP': label = 'HOP_'; break;
			  case 'SAN': label = 'HOP_'; break;
			  case 'PAA': label = 'PAA_'; break;
			  case 'HOT': label = 'HOT_'; break;
			  case 'VIL': label = 'VIL_'; break;
			  case 'ORP': label = 'MAI_'; break;
			  case 'RES': label = 'MAI_'; break;
			  case 'SPO': label = 'SPO_'; break;
			  case 'AER': label = 'CIA_'; break;
			  case 'SEM': label = 'OTH_'; break;
			  case 'OTH': label = 'OTH_'; break;
			  case 'MIL': label = 'MIL_'; break;
			  case 'PIS': label = 'PIS_'; break;
			  case 'GOU': label = 'OTH_'; break;
			  case 'SCO': label = 'MAI_'; break;
			  case 'RST': label = 'MAI_'; break;
			  default: label = 'HOT_'; break;
		  }	
	  }
  	  
	  if (label != ''){
		  result += label + color + '.png';
	  }		  
	  else{
		  result ='';  
	  }
	  return result;
  }
  
  function showFilter(){
	  var acc = document.getElementsByClassName("accordion");
	  var i;

	  for (i = 0; i < acc.length; i++) {
	    acc[i].addEventListener("click", function() {
	      this.classList.toggle("active");
	      var panel = this.nextElementSibling;
	      if (panel.style.display === "block") {
	        panel.style.display = "none";
	      } else {
	        panel.style.display = "block";
	      }
	    });
	  }
  }
  
  function checkUncheckAll(name, bool){
	  var checkboxes = document.getElementsByName(name);	    
	  for(var i=0; i<checkboxes.length;i++) {
		  checkboxes[i].checked = bool;
	  }
	  checkForUpdate();
  }
  
  function uncheckAll(){
	  var checkboxes = document.querySelectorAll('input[type=checkbox]');    
	  for(var i=0; i<checkboxes.length;i++) {
		  checkboxes[i].checked = false;
	  }
  }
  
  function resetFilters(){
	  var checkBoxes = document.querySelectorAll('input[type=checkbox]');
	  for (var j = 0; j < checkBoxes.length; j++) {	
		  checkBoxes[j].checked = true;
	  }
	  checkForUpdate();
  }
  
  function filter(){

	  var checkBoxes = document.querySelectorAll('input[type=checkbox]');
	  
	  for (i = 0; i < markers.length; i++) {
		  var setVisible = true;
		  marker = markers[i];		  
		  for (var j = 0; j < checkBoxes.length; j++) {	
			     if (!checkBoxes[j].checked) {
			        if (checkBoxes[j].value == marker.type_code || checkBoxes[j].value == marker.country_name
			        		|| checkBoxes[j].value == marker.status_code || checkBoxes[j].value == String(marker.nickname)){
			        	setVisible = false;
			        }
			        if (checkBoxes[j].name == 'checkRegion' && checkBoxes[j].value == marker.region_id){
			        	setVisible = false;
			        }
			        if (checkBoxes[j].name == 'checkDept' && checkBoxes[j].value == marker.department_id){
			        	setVisible = false;
			        }
			        if (checkBoxes[j].name == 'checkUrbex' && (/true/i).test(checkBoxes[j].value) == (/true/i).test(marker.is_urbex))
		        	{
			        	setVisible = false;
		        	}
			     }
		  }
		  google.maps.event.trigger(map, 'click');
		  marker.setVisible(setVisible);		  
	  }
	  var visibleMarkers = getVisibleMarkers();
	  if(visibleMarkers != 0){
		  $("#allList").attr("disabled", false).val('Liste des lieux');
	  }
	  else{
		  $("#allList").attr("disabled", true).val('Aucun marker s\u00e9l\u00e9ctionn\u00e9');		  
	  }
	  if(visibleMarkers != markers.length){
		  $("#resetFilters").attr("disabled", false).val('Reset filters');
	  }
	  else{
		  $("#resetFilters").attr("disabled", true).val('Aucun filtre activ\u00e9');
	  }
  }  
  
  function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
  }
  
  function getHighestZIndex() {
      for (var i=0; i<markers.length; i++) {
          tempZIndex = markers[i].getZIndex();
          if (tempZIndex>highestZIndex) {
              highestZIndex = tempZIndex;
          }
      }
      return highestZIndex;
  }

  function goToMarker(value){
	  tmpMarker.setMap(null);
	  for (i = 0; i < markers.length; i++) {		  
		  if(markers[i].title == value || markers[i].id == value){
			  if(markers[i].getVisible()){
				  map.setCenter(markers[i].getPosition());
				  map.setZoom(17);
				  google.maps.event.trigger(map, 'click');
				  google.maps.event.trigger(markers[i], 'click');
			  }
			  else{
				  alert('Marker non visible !');
			  }
		  }
	  }
  }
  
function goToCoords(user_type_id){	
	if ($("#searchMap").val() != null && $("#searchMap").val() != ''){
		tmpMarker.setMap(null);
		var coords = $("#searchMap").val().split(',');
		var pos = new google.maps.LatLng(coords[0], coords[1]);	 		
		tmpMarker.setPosition(pos);
		map.setCenter(tmpMarker.getPosition());
		map.setZoom(17);
		tmpMarker.setMap(map);
		
		if (parseInt(user_type_id) >= 4){
			var tmpIWContent = document.createElement('div');
			var add = document.createElement('a'); 
			add.textContent = 'Ajouter \u00e0 la carte';
			add.setAttribute('href', '/MyUrbex/addMarker.jsp?lat=' + coords[0] + '&lng=' + coords[1]);
			tmpIWContent.appendChild(add);		
			google.maps.event.addListener(tmpMarker, 'click', function() {				
				tmpInfowindow = new google.maps.InfoWindow({
				    content: tmpIWContent,
				    position: tmpMarker.getPosition()
				  });
				tmpInfowindow.open(map,tmpMarker);
			}); 
		}
		$("#searchMap").val('');
	}
}
  
function getVisibleMarkers(){
	var ids = '(';
	var count = 0;
	for (i = 0; i < markers.length; i++) {	
		if(markers[i].getVisible()){
			ids += "'" + markers[i].id + "',";
			count++;
			state = false;
		}
	}	
	if (count != 0){
		ids = ids.substring(0, ids.length-1);
		ids += ')';		
	}
	else{
		ids = null;			
	}
	$("#count").text('');
	$("#count").append('Nombre de lieux : ' + count);
	$("#ids").val(ids);
	$.ajax({
	    url:'PopulateListMarkers',
	    type:'POST',
	    data: {
	    	ids: ids
	    }
	  });
	return count;
}

function getUnvisibleMarkers(){
	var ids = '(';
	var count = 0;
	for (i = 0; i < markers.length; i++) {	
		if(!markers[i].getVisible()){
			ids += "'" + markers[i].id + "',";
			count++;
		}
	}	
	ids = ids.substring(0, ids.length-1);	  
	ids = ids.length == 0 ? '' : ids += ')';
	return ids;
}

function resetMap(){
	google.maps.event.trigger(map, 'click');
	map.setCenter(bounds.getCenter());
	map.fitBounds(bounds);
}
















