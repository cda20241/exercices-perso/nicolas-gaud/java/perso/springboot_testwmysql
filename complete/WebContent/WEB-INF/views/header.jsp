<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/animate.css"> 
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/master.css">      
        <link rel="stylesheet" href="css/slideshow.css">  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ"></script>
        <script src="scripts/jquery-ui.js"></script>
        <script src="scripts/modal.js"></script>
        <script src="scripts/scriptsMaps.js"></script>
        <script src="scripts/autocomplete.js"></script>
        <script src="scripts/slideshow.js"></script>
        <script src="scripts/user.js"></script>
    </head>
</html>