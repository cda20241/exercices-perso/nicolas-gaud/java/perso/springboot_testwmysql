<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
  		<title>My Urbex - All Map</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/animate.css"> 
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/master.css">        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ&libraries=places"></script>
        <script src="scripts/jquery-ui.js"></script>
        <script src="scripts/sidebar.js"></script>
        <script src="scripts/scriptsMaps.js"></script>
        <script src="scripts/autocomplete.js"></script>
        <script src="scripts/user.js"></script>    
  </head>
  <body ${sessionUser != null ? 'onload="initialize('.concat(sessionUser.user_type_id).concat(',').concat(param.id).concat(');"') : ''}>
	 	<div id="connectModal" class="modal ${sessionUser != null ? 'hide' : 'show'}">		
		  <div class="modal-content connect">
		    <h3>Se connecter</h3><br>
		    <h5>Veuillez remplir tous les champs</h5>		    
		    <form id="userLogin" action="connection" method="post">
		    	<div id="error" class="error"></div>    
		    	<input type="email" name="email" id="email" placeholder="Email" required>
		    	<input type="password" name="password" id="password" placeholder="Mot de passe" autocomplete="off" required>
		    	<a class="modal-link" onclick="toggleModal('mailModal');">Mot de passe oubli� ?</a>
		    	<button type="submit">Connexion</button>
		    </form>
		    <script>
			    $('#userLogin').submit(function(e) {
			    	e.preventDefault();
			    	doConnect();	
			   });
		    </script>
		  </div>		
		</div>
		<div id="mailModal" class="modal hide">		
		  <div class="modal-content mail">
		    <h3>R�cup�ration de mot de passe</h3><br>
		    <h5>Veuillez saisir votre email</h5>		    
		    <form id="userMdp" action="LostPassword" method="post">		    	
		    	<input type="email" name="email" id="emailMdp" placeholder="Email" required>		    
		    	<button type="submit">Envoyer</button>
		    </form>
		    <button onclick="toggleModal('mailModal')">Annuler</button>
		    <script>
			    $('#userMdp').submit(function(e) {
			    	e.preventDefault();
			    	sendPassword();	
			   });
		    </script>
		  </div>		
		</div>
		<div id="mailConfirmed" class="modal hide">		
		  <div class="modal-content confirmed">	
		    <h5 id="mailConfirmedRes"></h5>	    	    			    		   
		    <button onclick="toggleModal('mailConfirmed')">OK</button>
		  </div>		
		</div>
		<div id="insertConfirmed" class="modal hide">		
		  <div class="modal-content confirmed">	
		    <h5 id="insertConfirmedRes"  class="${not empty errorInsert ? errorInsert : ''}">${not empty insertResult ? insertResult : ''}</h5>	  	    			    		   
		    <button onclick="toggleModal('insertConfirmed');goToMarker('${newMarker.name}');">OK</button>
		  </div>		
		</div>
		<c:if test="${not empty insertResult}">
    		<script>toggleModal('insertConfirmed');</script>
    		<c:remove var="insertResult" scope="session" />
		</c:if>
		<div class="container-fluid" id="middle">
			<main class="ct" role="main">
				<div class="row row10">
					<div class="col-lg-3">
						<div id="title">
							<h1>Carte Urbex</h1><br>														
						</div>						
					</div>
					<div class="col-lg-6">
						<div class="welcome pulse" id="userWelcome">${sessionUser != null ? 'Bienvenue '.concat(sessionUser.nickname).concat(' !') : ''}</div>						
					</div>
					<div class="col-lg-3">
						<form action="/MyUrbex/populatelistmarkers" method="POST" target="_blank">
							<input type="hidden" id="ids" name="ids">
							<input id="allList" type="submit" onclick="getVisibleMarkers()" value="Liste des lieux">
						</form>							
					</div>
				</div>
				<div class="row row5">
					<div class="col-lg-3">											 
							<input type="submit" onclick="disconnect()" value="Deconnexion">
							<span>-</span>
							<input type="submit" onclick="window.location.href='/MyUrbex/parameters.jsp';" value="Param�tres">
					</div>
					<div class="col-lg-6">
						<input id="resetMap" type="submit" onclick="resetMap()" value="Centrer la carte">	
					</div>
					<div class="col-lg-3">
						<input id="resetFilters" type="submit" onclick="resetFilters()" value="Aucun filtre activ�" disabled>		
					</div>
				</div>
				<section class="content">
					<div class="sidebar"><%@include file="sidebar.jsp" %></div>	
					<div class="map_container">
						<form id="search" class="searchMaps" autocomplete="off">
							<div class="ui-widget">
			                	<input type="text" id="searchMap" name="searchMap" class="input" placeholder="Search.." autofocus accesskey="a"/>			    
			                	<button type="submit" id="btnSearch" ><i class="fa fa-search"></i></button>
			            	</div>
		            	</form>
		            	<script>
			   				$('#search').submit(function(e) {
			    				e.preventDefault();
			    				goToCoords('${sessionUser.user_type_id}');	
			   				});
		    			</script>
						<div id="map_canvas"></div>
					</div>	
				</section>		
			</main>
		</div>
  </body>
</html>