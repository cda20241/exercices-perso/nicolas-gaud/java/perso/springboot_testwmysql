<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>My Urbex - Paramètres</title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/animate.css"> 
	<link rel="stylesheet" href="css/custom.css"> 
	<link rel="stylesheet" href="css/master.css">        
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ&libraries=places"></script>
	<script src="scripts/jquery-ui.js"></script>
	<script src="scripts/sidebar.js"></script>
	<script src="scripts/scriptsMaps.js"></script>
	<script src="scripts/autocomplete.js"></script>
	<script src="scripts/user.js"></script>
</head>
<body>
	<script type="text/javascript">
		if ('${sessionUser}' == null || '${sessionUser}' == ''){
			document.location.href="/MyUrbex"	
		};
	</script>
	<div class="container-fluid" id="middle">
		<main class="ct" role="main">
			<div id="updateConfirmed" class="modal hide">		
			  <div class="modal-content confirmed">	
			    <h5 id="updateConfirmedRes"></h5>	    	    			    		   
			    <button onclick="window.location.href='/MyUrbex';">OK</button>
			  </div>		
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div id="title">
						<h1>Paramètres</h1>
					</div>
					<section class="content">
						<h2>Modifier vos informations</h2>
						<div class="default">
							<form id="userUpdate" class="form-horizontal" role="form" method="POST">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<div id="errorUpdate" class="error"></div>    
									</div>
									<div class="col-md-3"></div>
								</div>
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="nicknameUpdate">Pseudo : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="text" name="nicknameUpdate" id="nicknameUpdate" value="${sessionUser.nickname}">
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="emailUpdate">Adresse e-mail : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="email" name="emailUpdate" id="emailUpdate" value="${sessionUser.email}">
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>		
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="passwordUpdate">Mot de passe : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="password" name="passwordUpdate" id="passwordUpdate">
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="passwordUpdate2">Confirmation mot de passe : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="password" name="passwordUpdate2" id="passwordUpdate2">
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>																																	
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<button type="submit" class="btn btnLeft">Modifier</button>
										<button class="btn btnRight" onclick="window.location.href='/MyUrbex';">Annuler</button>
									</div>
									<div class="col-md-3"></div>
								</div>								 
							</form>
							<script>
							    $('#userUpdate').submit(function(e) {
							    	if ($('#nicknameUpdate').val() == '${sessionUser.nickname}' && 
							    			$('#emailUpdate').val() == '${sessionUser.email}' && 
							    			($('#passwordUpdate').val() == '' || $('#passwordUpdate').val() == '${sessionUser.password}')){
								    	alert('Aucune modification, rien \u00e0 faire.');
							    	}	
							    	else{
								    	e.preventDefault();
								    	updateUser();	
							    	}
							   });
						    </script>
						</div>
					</section>
				</div>
			</div>
		</main>
	</div>
</body>
</html>