<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
  		<title>My Urbex - Details</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css"> 
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/master.css">    
		<link rel="stylesheet" href="css/slideshow.css">     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ&libraries=places"></script>
        <script src="scripts/modal.js"></script>
        <script src="scripts/scriptsMaps.js"></script>
        <script src="scripts/slideshow.js"></script>

  </head>
  <body>
	<script type="text/javascript">
		$(document).ready(function(){			
			getImages(${marker.id});				
		});
	</script>
	<script type="text/javascript">
		if ('${sessionUser}' == null || '${sessionUser}' == ''){
			document.location.href="/MyUrbex"	
		};
	</script>
	<div class="container-fluid" id="middle">
		<main class="ct" role="main">
			<div class="row">
				<div class="col-lg-9">
					<div id="title">
						<h1>${marker.name}</h1><br>
						<h2>Type :&nbsp;</h2><h2 class="${marker.nickname != null ? 'green' : (marker.status_label.toLowerCase())}">${marker.type_label}</h2>
						<h2>&nbsp;-&nbsp;Status :&nbsp;</h2><h2 class="${marker.nickname != null ? 'green' : (marker.status_label.toLowerCase())}">${marker.status_label}</h2><br>
						<a href="${marker.maps_link}" target="_blank">Localisation : ${marker.latitude}, ${marker.longitude}
						<c:out default="None" escapeXml="true" value="${empty marker.department_name ? '' : ' - Dept : '.concat(marker.department_name).concat(' (').concat(marker.department_id).concat(')')}" /> 
						<c:out default="None" escapeXml="true" value="${empty marker.region_name ? '' : ' - Region : '.concat(marker.region_name)}" /> 
						 - Pays : ${marker.country_name}</a>
					</div>
				</div>
				<div class="col-lg-3">
					<a href="/MyUrbex/allMap.jsp?id=${marker.id}">Retour � la carte</a>
					<br>
					<a href="/MyUrbex/PopulateListMarkers?page=${currentPage != null ? currentPage : (sessionPage != null ? sessionPage : 1)}">Retour � la liste</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2">
					<c:if test = "${marker.id != minId}">						
						<a href="/MyUrbex/markerdetails?id=${marker.id}&dir=desc" class="aprev">&lt; Lieu pr�c�dent</a>
					</c:if>
				</div>
				<div class="col-lg-8">
					<div class="thumbContainer" id="thumbs"></div>
				</div>	
				<div class="col-lg-2">
					<c:if test = "${marker.id != maxId}">
						<a href="/MyUrbex/markerdetails?id=${marker.id}&dir=asc" class="anext">Lieu suivant &gt;</a>
					</c:if>
				</div>							
			</div>
			<section class="content">
				<div class="details">
					<div class="slideshow-container" id="images"></div>
				</div>	
			</section>		
		</main>
	</div>
  </body>
</html>